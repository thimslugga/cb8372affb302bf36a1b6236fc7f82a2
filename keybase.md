### Keybase proof

I hereby claim:

  * I am thimslugga on github.
  * I am thimslugga (https://keybase.io/thimslugga) on keybase.
  * I have a public key ASCbKvqyHBNw7GKM10LSh40gShapstPVCyn3fX-xj0Z6FQo

To claim this, I am signing this object:

```json
{
  "body": {
    "key": {
      "eldest_kid": "01209b2afab21c1370ec628cd742d2878d204a16a9b2d3d50b29f77d7fb18f467a150a",
      "host": "keybase.io",
      "kid": "01209b2afab21c1370ec628cd742d2878d204a16a9b2d3d50b29f77d7fb18f467a150a",
      "uid": "2e2da187f4cbe4414799603025e1b819",
      "username": "thimslugga"
    },
    "merkle_root": {
      "ctime": 1552653555,
      "hash": "aeb204b0371f9921d44d19144f1e4cbf009a99e5d183e0391e1bbe84b5a4f7385781f46b3e05410025eb65046d21f322422c9a213d4fb8b8a1820558837216cb",
      "hash_meta": "48098a38f7388b3ac98f20135f12ecad3d7e023b361f03e3dea6bdef9bd97075",
      "seqno": 4955198
    },
    "service": {
      "entropy": "+Uxeng6/H2KmDwlnS6H9Bi75",
      "name": "github",
      "username": "thimslugga"
    },
    "type": "web_service_binding",
    "version": 2
  },
  "client": {
    "name": "keybase.io go client",
    "version": "3.1.2"
  },
  "ctime": 1552653577,
  "expire_in": 504576000,
  "prev": "3c929feea9b4185087af3991aacee2d661196c49c19bf2073bc0e7b090d09aa4",
  "seqno": 4,
  "tag": "signature"
}
```

with the key [ASCbKvqyHBNw7GKM10LSh40gShapstPVCyn3fX-xj0Z6FQo](https://keybase.io/thimslugga), yielding the signature:

```
hKRib2R5hqhkZXRhY2hlZMOpaGFzaF90eXBlCqNrZXnEIwEgmyr6shwTcOxijNdC0oeNIEoWqbLT1Qsp931/sY9GehUKp3BheWxvYWTESpcCBMQgPJKf7qm0GFCHrzmRqs7i1mEZbEnBm/IHO8DnsJDQmqTEIEnQ+6t5Nay4vNBDUESHKzUmZQ7o/y2SBkkFpbYwPGitAgHCo3NpZ8RA3Vj6DW4QJgPl3DFWEDRDoDhuFWA5RfHgg5TT4ZjiVLKYojtOMjBpktxOutqF+Xu7LTJ19SpBKq6Km82XUkrfDqhzaWdfdHlwZSCkaGFzaIKkdHlwZQildmFsdWXEIFOTpINAaH5icv812tAzLueGuXm/12grNwd0rBplYGO5o3RhZ80CAqd2ZXJzaW9uAQ==

```

And finally, I am proving ownership of the github account by posting this as a gist.

### My publicly-auditable identity:

https://keybase.io/thimslugga

### From the command line:

Consider the [keybase command line program](https://keybase.io/download).

```bash
# look me up
keybase id thimslugga
```